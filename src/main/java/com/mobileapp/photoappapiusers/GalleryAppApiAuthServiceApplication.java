package com.mobileapp.photoappapiusers;

import feign.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
//@EnableCircuitBreaker
public class GalleryAppApiAuthServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(GalleryAppApiAuthServiceApplication.class, args);
    }

    /***
     * for BCryptPasswordEncoder to be injected into our UserServiceImpl via constructor its object needs to exist
     * in application context./create Bean of BCryptPasswordEncoder data type.
     * @return BCryptPasswordEncoder object
     */
    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /***
     * create RestTemplate as a bean.<p>
     * Bean Annotation - after annotating, this RestTemplate becomes available to our class as a bean & we can autowired it
     * @return new instance of RestTemplate/ RestTemplate object/bean
     */
    @Bean
    @LoadBalanced
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    @Bean
    Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }

    /***make FeignErrorDecoder available as a Bean.
     * @return an instance of FeignErrorDecoder as a Bean.
     */
//    @Bean
//    public FeignErrorDecoder getFeignErrorDecoder() {
//        return new FeignErrorDecoder();
//    }

    @Bean
    Throwable getCause() {
        return new Throwable();
    }

}