package com.mobileapp.photoappapiusers.proxy;

import com.mobileapp.photoappapiusers.ui.model.AlbumResponseModel;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/***
 * Feign Client for albums-ws
 */
@FeignClient(name = "albums-ws"
//        , fallback = AlbumServiceClientFallback.class
        , fallbackFactory = AlbumServiceClientFallbackFactory.class)
public interface AlbumServiceClient {

    @GetMapping(path = "/users/{id}/albums")
    public List<AlbumResponseModel> getAlbums(@PathVariable(value = "id") String userId);

}