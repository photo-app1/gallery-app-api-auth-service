package com.mobileapp.photoappapiusers.proxy;

import com.mobileapp.photoappapiusers.ui.model.AlbumResponseModel;
import feign.FeignException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Log4j2
public class AlbumServiceClientFallback implements AlbumServiceClient {

//    @Override
//    public List<AlbumResponseModel> getAlbums(String userId) {
//        return new ArrayList<>();
//    }

    private final Throwable cause;
    private final static String MSG = "exception found: {}";

    @Autowired
    public AlbumServiceClientFallback(Throwable cause) {
        this.cause = cause;
    }

    @Override
    public List<AlbumResponseModel> getAlbums(String userId) {

        if (cause instanceof FeignException && ((FeignException) cause).status() == 404) {
            log.error(MSG, "404 error took place when getAlbums was  called with userId:" + userId +
                    ". Error message: " + cause.getLocalizedMessage());
        } else {
            log.error(MSG, "other error took place : " + cause.getLocalizedMessage());
        }

        return new ArrayList<>();
    }

}