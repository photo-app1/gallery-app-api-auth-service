package com.mobileapp.photoappapiusers.proxy;

import com.mobileapp.photoappapiusers.exception.AuthServiceException;
import com.mobileapp.photoappapiusers.exception.BadRequestException;
import feign.Response;
import feign.codec.ErrorDecoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

/***
 * Implementation of Feign ErrorDecoder Interface - central place for handling all feign errors
 * (which feign client receives) by overriding decode method. business logic to handle different types of
 * errors that feign client can receive is centralize in one place here.
 */
@Component
public class FeignErrorDecoder implements ErrorDecoder {

    Environment env;

    @Autowired
    public FeignErrorDecoder(Environment env) {
        this.env = env;
    }

    /***
     *implementation for decode method. decode method will be called for all errors that feign client receives.
     * @param methodKey Feign client interface name plus the method name which was called
     * @param response http response object
     * @return create & throw a custom exception message
     */
    @Override
    public Exception decode(String methodKey, Response response) {

        //based on the status code can treat each http status code separately and for each return a specific error msg.
        switch (response.status()) {

            //handle http status code 400 errors & throw custom exception message
            case 400: {
                if (methodKey.contains("getAlbums")) {
                    throw new BadRequestException(response.reason());
                }
                break;
            }

            //handle http status code 404 errors & throw custom exception message
            case 404: {
                if (methodKey.contains("getAlbums")) {

                    /*ResponseStatusException is a one of the exception that spring framework provides which  we can
                    use to respond back with specific http status code and an error msg.*/
                    throw new ResponseStatusException(HttpStatus.valueOf(response.status()),
                            env.getProperty("albums.exceptions.albums-not-found"));
                }
                break;
            }

            default:
                return new Exception(response.reason());
        }
        return null;
    }
}