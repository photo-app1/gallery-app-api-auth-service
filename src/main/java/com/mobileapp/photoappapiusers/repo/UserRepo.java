package com.mobileapp.photoappapiusers.repo;

import com.mobileapp.photoappapiusers.data.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepo extends JpaRepository<User, Long> {

    /***
     * query method for find user record by email
     * @param username email
     * @return UserEntity object
     */
    Optional<User> findByEmail(String username);

    Optional<User> findByUserId(String userId);

}
