package com.mobileapp.photoappapiusers.exception;

import org.springframework.http.HttpStatus;
import org.springframework.lang.Nullable;

public class AuthServiceException extends RuntimeException {

    private static final long serialVersionUID = -4048916462298435481L;
    private HttpStatus statusCode;

    public AuthServiceException(String message, @Nullable HttpStatus statusCode) {
        super(message);
        this.statusCode = statusCode;
    }

    public HttpStatus getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(HttpStatus statusCode) {
        this.statusCode = statusCode;
    }
}
