package com.mobileapp.photoappapiusers.exception.handler;

import com.mobileapp.photoappapiusers.exception.AuthServiceException;
import com.mobileapp.photoappapiusers.exception.BadRequestException;
import com.mobileapp.photoappapiusers.ui.model.ErrorMsgResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Date;

/***
 * centralize place to catch & handle all exceptions.
 */
@ControllerAdvice
@Log4j2
public class AppExceptionsHandler extends ResponseEntityExceptionHandler {

    private final static String MSG = "exception found: {}";

    @ExceptionHandler(value = {Exception.class, ResponseStatusException.class})
    public ResponseEntity<ErrorMsgResponse> handleGeneralExceptions(Exception ex, WebRequest request) {

        log.error(MSG, ex.getLocalizedMessage());

        String errorMsgDescription = ex.getLocalizedMessage();
        if (errorMsgDescription == null) errorMsgDescription = ex.toString();
        ErrorMsgResponse errorMsg = new ErrorMsgResponse(new Date(), errorMsgDescription);

        return new ResponseEntity<>(errorMsg, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = AuthServiceException.class)
    public ResponseEntity<Object> handleAuthServiceExceptions(AuthServiceException ex, WebRequest request) {

        log.error(MSG, ex.getLocalizedMessage());

        String errorMsgDescription = ex.getLocalizedMessage();
        if (errorMsgDescription == null) errorMsgDescription = ex.toString();
        ErrorMsgResponse errorMsg = new ErrorMsgResponse(new Date(), errorMsgDescription);

        return new ResponseEntity<>(errorMsg, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = BadRequestException.class)
    public ResponseEntity<Object> handleBadRequestExceptions(BadRequestException ex, WebRequest request) {

        log.error(MSG, ex.getLocalizedMessage());

        String errorMsgDescription = ex.getLocalizedMessage();
        if (errorMsgDescription == null) errorMsgDescription = ex.toString();
        ErrorMsgResponse errorMsg = new ErrorMsgResponse(new Date(), errorMsgDescription);

        return new ResponseEntity<>(errorMsg, HttpStatus.BAD_REQUEST);
    }

}
