package com.mobileapp.photoappapiusers.service.impl;

import com.mobileapp.photoappapiusers.proxy.AlbumServiceClient;
import com.mobileapp.photoappapiusers.data.User;
import com.mobileapp.photoappapiusers.exception.AuthServiceException;
import com.mobileapp.photoappapiusers.repo.UserRepo;
import com.mobileapp.photoappapiusers.shared.UserDto;
import com.mobileapp.photoappapiusers.service.AuthService;
import com.mobileapp.photoappapiusers.ui.model.AlbumResponseModel;
import com.mobileapp.photoappapiusers.ui.model.CreateUserResponseModel;
import com.mobileapp.photoappapiusers.ui.model.ErrorMessages;
import com.mobileapp.photoappapiusers.ui.model.GetUserResponseModel;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Log4j2
@Service
public class AuthServiceImpl implements AuthService {

    UserRepo userRepo;
    BCryptPasswordEncoder bCryptPasswordEncoder;
    RestTemplate restTemplate;
    AlbumServiceClient albumServiceClient;
    Environment env;

    @Autowired
    public AuthServiceImpl(
            UserRepo userRepo,
            BCryptPasswordEncoder bCryptPasswordEncoder,
            RestTemplate restTemplate,
            AlbumServiceClient albumServiceClient, Environment env) {
        this.userRepo = userRepo;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.restTemplate = restTemplate;
        this.env = env;
        this.albumServiceClient = albumServiceClient;
    }

    public static final String MSG = "exception found: {}";

    @Override
    public CreateUserResponseModel createUser(UserDto userDetails) {

        userDetails.setUserId(UUID.randomUUID().toString());
        userDetails.setEncryptedPassword(bCryptPasswordEncoder.encode(userDetails.getPassword()));

        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        User persistUserDetails = modelMapper.map(userDetails, User.class);

        User savedUser = userRepo.save(persistUserDetails);

        return modelMapper.map(savedUser, CreateUserResponseModel.class);
    }

    /**
     * 6.
     * when spring framework is trying to authenticate the user it will look for this method.
     *
     * @param username username(email) provided during login
     * @return spring security object(UserDetails) that have user details that match username provided during login
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User userRecord = userRepo.findByEmail(username)
                .orElseThrow(() -> new UsernameNotFoundException("can't find user for this email :" + username));

        List<SimpleGrantedAuthority> roles = Arrays.asList(new SimpleGrantedAuthority(userRecord.getRole()));

        return new org.springframework.security.core.userdetails.User(userRecord.getEmail(), userRecord.getEncryptedPassword(), true, true,
                true, true, roles);
    }


    /**
     * 8.
     * get user details using email address for add to jwt token in AuthenticationFilter
     *
     * @param email email
     * @return UserDto object
     */
    @Override
    public UserDto getUserDetailsByEmail(String email) {

        User userEntity = userRepo.findByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException("can't find user for this email :" + email));
        return new ModelMapper().map(userEntity, UserDto.class);
    }

    @Override
    public GetUserResponseModel getUser(String userId) {

        User user = userRepo.findByUserId(userId)
                .orElseThrow(() -> new AuthServiceException(ErrorMessages.NO_RECORD_FOUND.getErrorMsg() + userId,
                        HttpStatus.INTERNAL_SERVER_ERROR));

        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        GetUserResponseModel userResponseModel = modelMapper.map(user, GetUserResponseModel.class);

//        String albumUrl = String.format(env.getProperty("restTemplate.albums.url"), userId);
//
//        ResponseEntity<List<AlbumResponseModel>> albumListResponse = restTemplate.exchange(
//                albumUrl, HttpMethod.GET, null, new ParameterizedTypeReference<List<AlbumResponseModel>>() {
//                });
//        List<AlbumResponseModel> albumList = albumListResponse.getBody();

//        List<AlbumResponseModel> albumList = null;
//        try {
//            albumList = albumServiceClient.getAlbums(userId);
//        } catch (Exception ex) {
//            log.error(MSG, ex.getLocalizedMessage());
//        }

        log.info("Before calling albums-ws microservice");
        List<AlbumResponseModel> albumList = albumServiceClient.getAlbums(userId);
        log.info("After calling albums-ws microservice");

        userResponseModel.setAlbums(albumList);
        return userResponseModel;

    }

}