package com.mobileapp.photoappapiusers.service;

import com.mobileapp.photoappapiusers.shared.UserDto;
import com.mobileapp.photoappapiusers.ui.model.CreateUserResponseModel;
import com.mobileapp.photoappapiusers.ui.model.GetUserResponseModel;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface AuthService extends UserDetailsService {

    CreateUserResponseModel createUser(UserDto userDetails);

    UserDto getUserDetailsByEmail(String userName);

    GetUserResponseModel getUser(String userId);
}
