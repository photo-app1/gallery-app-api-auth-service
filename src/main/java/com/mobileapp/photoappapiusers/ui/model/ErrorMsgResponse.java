package com.mobileapp.photoappapiusers.ui.model;

import java.util.Date;

public class ErrorMsgResponse {

    private Date timeStamp;
    private String errorMsg;

    public ErrorMsgResponse() {
    }

    public ErrorMsgResponse(Date timeStamp, String errorMsg) {
        this.timeStamp = timeStamp;
        this.errorMsg = errorMsg;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
