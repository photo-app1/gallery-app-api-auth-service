package com.mobileapp.photoappapiusers.ui.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class CreateUserRequestModel implements Serializable {

    private static final long serialVersionUID = 1796029781682477375L;

    @NotNull(message = "first name cannot be null")
    @Size(min = 2, message = "first name must be greater than 2 characters")
    private String firstName;

    @NotNull(message = "last name cannot be null")
    @Size(min = 2, message = "last name must be greater than 2 characters")
    private String lastName;

    @NotNull(message = "email cannot be null")
    @Email
    private String email;

    @NotNull(message = "password cannot be null")
    @Size(min = 8, max = 16, message = "password should be between 8 and 16 characters")
    private String password;

    @NotNull(message = "role cannot be null")
    @Size(min = 2, message = "role must be greater than 2 characters")
    private String role;


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
