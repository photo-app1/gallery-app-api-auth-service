package com.mobileapp.photoappapiusers.ui.controllers;

import com.mobileapp.photoappapiusers.service.AuthService;
import com.mobileapp.photoappapiusers.shared.UserDto;
import com.mobileapp.photoappapiusers.ui.model.CreateUserRequestModel;
import com.mobileapp.photoappapiusers.ui.model.CreateUserResponseModel;
import com.mobileapp.photoappapiusers.ui.model.GetUserResponseModel;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Size;

@RestController
@Validated
@RequestMapping("/users")
public class AuthController {

    @Autowired
    private Environment env;
    @Autowired
    private AuthService authService;

    /***
     * check running port.
     * @return
     */
    @GetMapping("/status/check")
    public String status() {
        return "working on port: " + env.getProperty("local.server.port");
    }

    /***
     * user registration/sign up.
     * @param userDetails user details
     * @return responseEntity object with CreateUserResponseModel object in the body
     */
    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<CreateUserResponseModel> createUser(@Valid @RequestBody CreateUserRequestModel userDetails) {

        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        UserDto userDetailsDto = modelMapper.map(userDetails, UserDto.class);
        CreateUserResponseModel createdUser = authService.createUser(userDetailsDto);

        return ResponseEntity.status(HttpStatus.CREATED).body(createdUser);
    }

    /***
     * get user details with user's albums
     * @param userId user id
     * @return responseEntity object with GetUserResponseModel object in the body
     */
    @GetMapping(path = "/{id}",
            consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<GetUserResponseModel> getUser(@PathVariable(value = "id")
                                                            @Size(min = 36, max = 36) String userId) {

        GetUserResponseModel returnedUser = authService.getUser(userId);
        return ResponseEntity.status(200).body(returnedUser);
    }

}