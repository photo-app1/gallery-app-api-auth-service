package com.mobileapp.photoappapiusers.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mobileapp.photoappapiusers.service.AuthService;
import com.mobileapp.photoappapiusers.shared.UserDto;
import com.mobileapp.photoappapiusers.ui.model.LoginRequestModel;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.core.userdetails.*;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

/**
 * 1.
 * attempt authentication - when login performed this filter class triggered and spring framework attempt to perform user authentication by calling attemptAuthentication method.
 */
public class AuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private AuthService authService;
    private Environment env;

    @Autowired
    public AuthenticationFilter(AuthService authService, Environment env, AuthenticationManager authenticationManager) {
        this.authService = authService;
        this.env = env;
        super.setAuthenticationManager(authenticationManager);
    }

    /**
     * 2.
     * attemptAuthentication method will helps us to read username & password from the request and to call the  authentication method on the authentication manager.
     *
     * @param request  LoginRequestModel Object
     * @param response
     * @return
     * @throws AuthenticationException
     */
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {
        try {

            LoginRequestModel creds = new ObjectMapper().readValue(request.getInputStream(), LoginRequestModel.class);
            return getAuthenticationManager().authenticate(
                    new UsernamePasswordAuthenticationToken(
                            creds.getEmail(),
                            creds.getPassword(),
                            new ArrayList<>()
                    ));

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 7.
     * success authentication - if authentication is successful spring framework will call this method itself.
     * this method take user details & then use those user details to generate jwt token. then add jwt token to http
     * response header and return it back with http response.
     *
     * @param request    user details
     * @param response
     * @param chain
     * @param authResult using this we can read the username of currently logged in/authenticated user
     * @throws IOException
     * @throws ServletException
     */
    @Override
    protected void successfulAuthentication(HttpServletRequest request,
                                            HttpServletResponse response,
                                            FilterChain chain,
                                            Authentication authResult) throws IOException, ServletException {
        //get user details
        String userName = ((User) authResult.getPrincipal()).getUsername();
        UserDto userDetails = authService.getUserDetailsByEmail(userName);

        //use that user details for generate JWT token / generate JWT token
        String token = Jwts.builder()
                .setSubject(userDetails.getUserId())
                .setExpiration(new Date(System.currentTimeMillis() + Long.parseLong((env.getProperty(
                        "token.expiration_time")))))
                .setIssuedAt(new Date())
                .signWith(SignatureAlgorithm.HS512, env.getProperty("token.secret.key"))
                .compact();

        response.addHeader("token", token);
        response.addHeader("userId", userDetails.getUserId());
    }

}

