package com.mobileapp.photoappapiusers.security;

import com.mobileapp.photoappapiusers.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurity extends WebSecurityConfigurerAdapter {

    private Environment env;
    private AuthService authService;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public WebSecurity(Environment env, AuthService authService, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.env = env;
        this.authService = authService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // since we are going to use JWT for our user authorization we also disable cross-site-request-forgery
        http.csrf().disable();
        // configure HTTPSecurity object to permit requests sent to a certain url paths.
        http.authorizeRequests()
                //.antMatchers("/users/**").permitAll()
                .antMatchers("/**").hasIpAddress(env.getProperty("gateway.ip"))
                .and()
                // 4. register our authentication filter class(our own custom filter) with http security.
                .addFilter(getAuthenticationFilter());
        // disable frame options for load h2 console(because h2 console runs within the frame).
        http.headers().frameOptions().disable();

    }

    /**
     * 3.
     * local method that creates custom AuthenticateFiler.
     *
     * @return authenticationFilter object
     * @throws Exception
     */
    private AuthenticationFilter getAuthenticationFilter() throws Exception {
        AuthenticationFilter authenticationFilter = new AuthenticationFilter(authService, env, authenticationManager());
        // authenticationFilter.setAuthenticationManager(authenticationManager());
        authenticationFilter.setFilterProcessesUrl(env.getProperty("login.url.path"));
        return authenticationFilter;
    }

    /**
     * 5.
     * to perform identification for help to attemptAuthentication we need to help spring framework to find user details.
     *
     * @param auth
     * @throws Exception
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(authService).passwordEncoder(bCryptPasswordEncoder);
    }

}