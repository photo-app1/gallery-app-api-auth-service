FROM openjdk:11
VOLUME /tmp
COPY build/libs/gallery-app-api-auth-service-0.0.1-SNAPSHOT.jar GalleryAppApiAuthService.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","GalleryAppApiAuthService.jar"]
